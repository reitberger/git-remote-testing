local Commit = {
    cc = "Create commit",
    ca = "commit --amend",
    ce = "commit --amend --no-edit",
}
local History = {
    -- Ended with space | neovim config starts with space to search in telescope
    cb = "populate command line with 'git branch'",
    co = "git checkout",
    cz = "git stash",
    cm = "git merge",
    c  = "git commit",
    r  = "git rebase",
    cr = "git revert",
    -- Does not end with space
    coo = "Check out pointer under cursor",
    ri = "rebase interactive",
}
local own = {
    ctrl_g = "Open git window",
    ctrl_h = "Open history (git log --oneline)",
    ctrl_r = "Open reflog",
    ctrl_b = "Open branch"
}
